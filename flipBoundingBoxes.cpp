/*
flipBoundingBoxes.cpp

Tim Ambrose
Flips the bounding box file horizontally, edits the location of the x coord
*/

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>

using std::string;
using std::stringstream;
using std::ofstream;
using std::ifstream;
using std::cout;

int main(int argc, char *argv[]) {
  if (argc < 3) {
    fprintf(stderr, "Usage: %s oldFilename newFilename\n", argv[0]);
    exit(-1);
  }
  ifstream inFile(argv[1]);
  ofstream outFile(argv[2]);
  string line;
  while (std::getline(inFile, line)) {
    size_t pos = line.find(" 0.");
    if (pos != string::npos) {
      size_t after;
      double xCoord = std::stod(line.substr(pos), &after);
      double newCoord = 1.0 - xCoord;
      cout << xCoord << " -> " << newCoord << "\n";
      outFile << line.substr(0, pos + 1) << std::fixed << std::setprecision(6)
              << newCoord << line.substr(pos + 9, string::npos) << "\n";
    }
  }
    
  return 0;
}
