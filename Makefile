CC= g++
CFLAGS=-std=c++11
LIBS=

all: flipBoundingBoxes

flipBoundingBoxes:	flipBoundingBoxes.cpp
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

############################################
clean:
	rm -f flipBoundingBoxes *.o *DS_STORE *.dSYM

